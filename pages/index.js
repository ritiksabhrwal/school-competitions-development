import Head from "next/head";
import Image from "next/image";
import Hero from "@/components/HeroSection";
import Section from "@/components/Section";
import Card from "@/components/Cards/homeCard";
import Button from "@/components/Button";
import styles from "@/styles/Home.module.css";
import {
  ArrowRightIcon,
  ChevronDoubleRightIcon,
  UserPlusIcon,
  TrophyIcon,
  RocketLaunchIcon,
} from "@heroicons/react/24/outline";

export default function Home() {
  return (
    <>
      <Head>
        <title>School Competitions</title>
        <meta
          name="description"
          content="School Competitions - A branch of Markall"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className={styles.hero}>
          <Hero imgSrc="/assets/images/home/heroImg.svg">
            <div className={`heroText ${styles.heroText}`}>
              <div>
                <Button
                  style={{
                    backgroundColor: "var(--secondary-color)",
                    color: "var(--primary-color)",
                    fontWeight: "700",
                  }}
                >
                  SchoolCompetitions.online
                  <ChevronDoubleRightIcon className="icon" />
                </Button>
              </div>
              <h2>Welcome To</h2>
              <h3>School Competitions</h3>
              <h4>Competitions among schools is always a great Motivator.</h4>
              <Button
                style={{ backgroundColor: "transparent", color: "initial" }}
              >
                <span>Explore Contests</span>
                <ArrowRightIcon className="icon" />
              </Button>
            </div>
          </Hero>
        </div>
        <section className="section bgPrimary">
          <div className={styles.quoteImage}>
            <Image
              src="/assets/images/home/sadhguruImg.svg"
              fill
              alt="Sadhguru Image"
            />
            <span>Sadhguru</span>
          </div>
          <div className={`text`}>
            <p className={styles.quoteText}>
              “ Competition will make children determined and focused in one
              way, at the same time make them fearful of failure, fearful of
              being less than somebody else. Here you will see they don't have
              that at all in them. Every one of them is a king by himself. ”
            </p>
          </div>
        </section>
        <Section imgSrc="/assets/images/home/progress.svg" reverse>
          <div>
            <p>
              Excellence is a gradual result
              <br />
              of always striving to do better
            </p>
            <h3 style={{ paddingLeft: "10%" }}>
              No <span>Competition</span>,
            </h3>
            <h3 style={{ paddingLeft: "50%" }}>
              No <span>Progress</span>.
            </h3>
          </div>
        </Section>
        <Section imgSrc="/assets/images/home/challenges.svg">
          <div>
            <p>
              <span>Challenges</span> are an
              <br />
              opportunity to test you & rise
              <br />
              to the next level.
            </p>
          </div>
        </Section>
        <Section imgSrc="/assets/images/home/competition.svg" reverse>
          <div>
            <p>
              There is always <span>competition</span>
              <br />
              out there.
              <br />
              That's what makes life exciting.
            </p>
          </div>
        </Section>
        <Section imgSrc="/assets/images/home/participation.svg">
          <div>
            <p>
              It is not always about winning...
              <br />
              Sometimes even
              <br />
              <span>participation</span> matters.
            </p>
          </div>
        </Section>
        <section className="section reverse">
          <div className={`text ${styles.runningText}`}>
            <p>
              Life is Better when you are <span>Running...</span>
            </p>
            <h5 className={styles.underline} style={{ marginBottom: "3vw" }}>
              Here we keep you running always.
            </h5>
            <h4>Signup & Stay Tuned</h4>
            <div className={styles.btns}>
              {["Individual", "School", "Organiser/ Sponser"].map(
                (label, index) => (
                  <Button
                    style={{ backgroundColor: "#000", color: "#fff" }}
                    key={`index-${index}`}
                  >
                    {label}
                  </Button>
                )
              )}
            </div>
          </div>
          <div className={styles.cardContainer}>
            <Card
              label="SignUp now & Stay Tuned"
              icon={<UserPlusIcon className="icon-lg" />}
            />
            <Card
              label="Lots of competitions coming"
              icon={<RocketLaunchIcon className="icon-lg" />}
            />
            <Card
              label="Lots of opportunities to win"
              icon={<TrophyIcon className="icon-lg" />}
            />
          </div>
        </section>
        <section
          className="section"
          style={{ backgroundColor: "var(--background-color)" }}
        >
          <div className={styles.contestContainer}>
            <div className={`relative ${styles.contestLogo}`}>
              <Image
                src="/assets/images/home/contestLogo.svg"
                alt="Image"
                fill
              />
            </div>
            <div className="text text-white" style={{ alignItems: "start" }}>
              <h6>Let's participate in</h6>
              <h1 style={{ color: "#ffffff70" }}>
                Online Nutrition
                <br />
                Photo <span style={{ color: "#fff" }}>Contest</span>
              </h1>
              <h6>Statrtign from 10 June,2023 To 11 June,2023</h6>
              <Button className="primaryBtn">
                Register & Participate
                <ChevronDoubleRightIcon className="icon" />
              </Button>
            </div>
          </div>
          <div className={`relative ${styles.contestImage}`}></div>
        </section>
        <div className={styles.terms}>
          <h6>
            <span>Terms & Conditions: </span>There are many variations of
            passages of Lorem Ipsum available, but the majority have suffered
            alteration in some form, by injected humour, orem Ipsum available,
            but the majority have suffered alteration in some form, by injected
            humour,.
          </h6>
        </div>
      </main>
    </>
  );
}
