import Layout from "@/components/Layout";
import "@/styles/globals.css";
import { Ubuntu, Poppins } from "next/font/google";

const openSans = Ubuntu({
  subsets: ["latin"],
  variable: "--font-primary",
  weight: ["400", "500", "700"],
});

const rubik = Poppins({
  subsets: ["latin"],
  variable: "--font-secondary",
  weight: ["100", "400", "500", "700", "900"],
});

export default function App({ Component, pageProps }) {
  const getLayout = Component.getLayout;
  if (getLayout) {
    return getLayout(
      <div className={`${openSans.variable} ${rubik.variable}`}>
        <Component {...pageProps} />
      </div>
    );
  } else {
    return (
      <Layout>
        <Component {...pageProps} />
      </Layout>
    );
  }
}
