import Head from "next/head";
import Register from "@/components/Auth/Register";

const register = () => {
  return (
    <>
      <Head>
        <title>School Competitions - Registration</title>
        <meta
          name="description"
          content="School Competitions Registration Form - A branch of Markall"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Register />
      </main>
    </>
  );
};

register.getLayout = function getLayout(page) {
  return page;
};

export default register;
