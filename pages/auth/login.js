import Head from "next/head";
import Login from "@/components/Auth/Login";

const login = () => {
  return (
    <>
      <Head>
        <title>School Competitions - Login</title>
        <meta
          name="description"
          content="School Competitions Login Form - A branch of Markall"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Login />
      </main>
    </>
  );
};

login.getLayout = function getLayout(page) {
  return page;
};

export default login;
