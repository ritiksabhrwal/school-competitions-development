import Image from "next/image";
import styles from "./Section.module.css";
const index = ({
  imgSrc,
  reverse,
  columnReverse,
  children,
  primaryColor,
  className,
}) => {
  return (
    <section
      className={`${styles.container} ${primaryColor && styles.primaryColor} ${
        className && styles[className]
      } ${reverse && styles.reverse}`}
    >
      <div
        className={`${styles.section} ${reverse && styles.reverse} ${
          columnReverse && styles.columnReverse
        }`}
      >
        <div className={`${styles.sectionText}`}>{children}</div>
        {imgSrc && (
          <div className={styles.image}>
            <Image src={imgSrc} fill alt="image" />
          </div>
        )}
      </div>
    </section>
  );
};

export default index;
