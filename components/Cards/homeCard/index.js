import styles from "./style.module.css";

const index = ({ icon, label }) => {
  return (
    <div className={styles.container}>
      <p>{label}</p>
      <div className={styles.iconContainer}>{icon}</div>
    </div>
  );
};

export default index;
