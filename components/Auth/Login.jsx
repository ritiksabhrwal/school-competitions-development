"use client";
import * as yup from "yup";
import { Form, Formik } from "formik";
import styles from "./style.module.css";
import { useRef, useState } from "react";
import SocialIcons from "@/components/SocialIcons";
import TextField from "../Formik/inputs/TextField";
import Button from "@/components/Button";

const Login = () => {
  const formikRef = useRef(null);
  const [loading, setLoading] = useState(false);
  const [rember, setRember] = useState(false);

  const InitialValues = {
    userName: "",
    password: "",
  };

  const ValidationSchema = yup.object().shape({
    userName: yup.string().required().label("Username"),
    password: yup.string().required().label("Password"),
  });

  const handleSubmit = (formData) => {
    if (!rember) {
      console.log("Please accept term and conditions.");
      return;
    }
    console.log(formData);
  };
  return (
    <div className={`${styles.container} ${styles.login}`}>
      <div className={styles.greetings}>
        <h4>LOGO.</h4>
        <h2>Welcome</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod dolor
          distinctio consectetur aspernatur ab illum!
        </p>
        <SocialIcons />
      </div>
      <div className={styles.formContainer}>
        <Formik
          initialValues={InitialValues}
          validationSchema={ValidationSchema}
          onSubmit={handleSubmit}
          innerRef={formikRef}
        >
          <Form className={styles.form}>
            <h3>Login Page</h3>
            <div className={styles.inputs}>
              <TextField name="userName" label="UserName" />
              <TextField name="password" label="Password" />
            </div>
            <div className={styles.terms}>
              <div
                className={rember ? styles.accepted : ""}
                onClick={() => setRember(!rember)}
              />
              <h6>Remeber me</h6>
            </div>
            <div className={styles.submitBtn}>
              <Button type="submit" disabled={loading}>
                Login
              </Button>
              <h3>
                Don't have an account? <a href="/auth/register">Create one</a>
              </h3>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
};

export default Login;
