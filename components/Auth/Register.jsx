"use client";
import * as yup from "yup";
import { Field, Form, Formik } from "formik";
import styles from "./style.module.css";
import { useRef, useState } from "react";
import SocialIcons from "@/components/SocialIcons";
import TextField from "../Formik/inputs/TextField";
import Button from "@/components/Button";

const Register = () => {
  const formikRef = useRef(null);
  const [loading, setLoading] = useState(false);
  const [profile, setProfile] = useState("student");
  const [tAndC, setTAndC] = useState(false);

  const InitialValues = {
    userName: "",
    email: "",
    password: "",
    confirmPassword: "",
    profile: "student",
  };

  const ValidationSchema = yup.object().shape({
    userName: yup.string().required().label("Username"),
    email: yup.string().email().required().label("Email"),
    password: yup.string().required().label("Password"),
    confirmPassword: yup.string().required().label("Confirm Password"),
    profile: yup.string(),
  });

  const handleSubmit = (formData) => {
    if (!tAndC) {
      console.log("Please accept term and conditions.");
      return;
    }
    console.log(formData);
  };
  return (
    <div className={`${styles.container}`}>
      <div className={styles.greetings}>
        <h4>LOGO.</h4>
        <h2>Welcome</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod dolor
          distinctio consectetur aspernatur ab illum!
        </p>
        <SocialIcons />
      </div>
      <div className={styles.formContainer}>
        <Formik
          initialValues={InitialValues}
          validationSchema={ValidationSchema}
          onSubmit={handleSubmit}
          innerRef={formikRef}
        >
          <Form className={styles.form}>
            <div className={styles.toggle}>
              <div role="group" className={styles.toggle__wrapper}>
                <div className={styles.toggle__options}>
                  <label
                    className={`${styles.toggle__field} ${
                      profile === "student" ? styles.activeProfile : ""
                    }`}
                    onClick={() => setProfile("student")}
                  >
                    <Field type="radio" name="profile" value="student" />
                    Student
                  </label>
                  <label
                    className={`${styles.toggle__field} ${
                      profile === "teacher" ? styles.activeProfile : ""
                    }`}
                    onClick={() => setProfile("teacher")}
                  >
                    <Field type="radio" name="profile" value="teacher" />
                    Teacher
                  </label>
                </div>
                <div className={styles.toggle__selector} />
              </div>
            </div>
            <h3>
              Register as a {profile.charAt(0).toUpperCase() + profile.slice(1)}
            </h3>
            <div className={styles.inputs}>
              <TextField name="userName" label="UserName" />
              <TextField name="email" label="Email address" />
              <TextField name="password" label="Password" />
              <TextField name="confirmPassword" label="Confirm Password" />
            </div>
            <div className={styles.terms}>
              <div
                className={tAndC ? styles.accepted : ""}
                onClick={() => setTAndC(!tAndC)}
              />
              <h6>Please read & accept to our Terms & Conditions</h6>
            </div>
            <div className={styles.submitBtn}>
              <Button type="submit" disabled={loading}>
                Register
              </Button>
              <h3>
                Already have an account? <a href="/auth/login">Login</a>
              </h3>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
};

export default Register;
