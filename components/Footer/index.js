import styles from "./style.module.css";
import SocialIcons from "@/components/SocialIcons";

const index = () => {
  const links = [
    { label: "Home", href: "/" },
    { label: "Contests", href: "/" },
    { label: "Terms & conditions", href: "/" },
    { label: "Privacy policy", href: "/" },
    { label: "About", href: "/" },
  ];
  return (
    <footer className={styles.container}>
      <SocialIcons />
      <div className={styles.links}>
        {links.map(({ label, href }, index) => (
          <a href={href} key={index}>
            <p>{label}</p>
          </a>
        ))}
      </div>
      <div className={styles.copy}>
        <span>&copy; SchoolCompetitions || All rights reserved</span>
      </div>
    </footer>
  );
};

export default index;
