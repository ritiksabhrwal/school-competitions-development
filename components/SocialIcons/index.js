import Image from "next/image";
import styles from "./style.module.css";

const index = () => {
  const icons = [
    { src: "fb", href: "/" },
    { src: "insta", href: "/" },
    { src: "linkedIn", href: "/" },
  ];
  return (
    <div className={styles.socialIcons}>
      {icons.map((icon, index) => (
        <div className={styles.icon} key={index}>
          <a
            href={icon.href}
            //   target="_blank"
          >
            <Image
              src={`/assets/icons/${icon.src}.svg`}
              alt={`${icon.src} icon`}
              fill
            />
          </a>
        </div>
      ))}
    </div>
  );
};

export default index;
