import Image from "next/image";
import styles from "./Hero.module.css";

const Hero = ({ children, imgSrc, className, columnReverse }) => {
  return (
    <header
      className={`${styles.heroSection} ${className && className} ${
        columnReverse && styles[columnReverse]
      }`}
    >
      {imgSrc && (
        <div className={styles.heroImage}>
          <Image src={imgSrc} fill alt="Image" priority />
        </div>
      )}
      {children}
    </header>
  );
};

export default Hero;
