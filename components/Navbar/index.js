"use client";
import styles from "./Navbar.module.css";
import Button from "@/components/Button";
import Link from "next/link";
import { useState } from "react";

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [active, setActive] = useState("");
  return (
    <div className={styles.navbar}>
      <h4>Logo</h4>
      <div>
        <ul
          className={`${styles.navMenu} ${!isOpen ? "" : `${styles.active}`}`}
          onClick={() => setIsOpen(false)}
        >
          <li
            className={`${styles.navItem} ${
              active === "home" ? styles.activePage : ""
            }`}
            onClick={() => setActive("home")}
          >
            <Link href="/">Home</Link>
          </li>
          <li
            className={`${styles.navItem} ${
              active === "categories" ? styles.activePage : ""
            }`}
            onClick={() => setActive("categories")}
          >
            <Link href="categories">Categories</Link>
          </li>
          <li
            className={`${styles.navItem} ${
              active === "contact" ? styles.activePage : ""
            }`}
            onClick={() => setActive("contact")}
          >
            <Link href="contact">Contact</Link>
          </li>
          <li
            className={`${styles.navItem} ${
              active === "about" ? styles.activePage : ""
            }`}
            onClick={() => setActive("about")}
          >
            <Link href="about">About</Link>
          </li>
          <li>
            <Button href="/auth/register">Sign Up</Button>
          </li>
        </ul>
        <button
          className={
            isOpen === false
              ? styles.hamburger
              : styles.hamburger + " " + styles.active
          }
          onClick={() => setIsOpen(!isOpen)}
        >
          <span className={styles.bar} />
          <span className={styles.bar} />
          <span className={styles.bar} />
        </button>
      </div>
    </div>
  );
};

export default Navbar;
