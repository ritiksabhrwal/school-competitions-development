import Navbar from "@/components/Navbar";
import styles from "./Layout.module.css";
import ScrollUp from "@/components/ScrollUp";
import Footer from "@/components/Footer";
import "react-toastify/dist/ReactToastify.css";
import { Ubuntu, Poppins } from "next/font/google";
import { ToastContainer } from "react-toastify";

const openSans = Ubuntu({
  subsets: ["latin"],
  variable: "--font-primary",
  weight: ["400", "500", "700"],
});

const rubik = Poppins({
  subsets: ["latin"],
  variable: "--font-secondary",
  weight: ["100", "400", "500", "700", "900"],
});

const Layout = ({ children }) => {
  return (
    <div
      className={`${openSans.variable} ${rubik.variable} ${styles.container}`}
    >
      <ToastContainer />
      <Navbar />
      {children}
      <Footer />
      <ScrollUp />
    </div>
  );
};

export default Layout;
