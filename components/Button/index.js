import styles from "./style.module.css";

const index = ({
  type = "button",
  href,
  disabled,
  className,
  children,
  ...props
}) => {
  return (
    <button
      className={`${styles.btn} ${className && className}`}
      type={type}
      disabled={disabled}
      {...props}
    >
      {href ? <a href={href}>{children}</a> : children}
    </button>
  );
};

export default index;
