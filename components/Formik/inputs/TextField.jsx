import styles from "./style.module.css";
import { ErrorMessage, useField } from "formik";

const TextField = ({ label, style, prefix, placeholder, ...props }) => {
  const [field, meta] = useField(props);

  const error = meta.touched && meta.error;

  const containerClass = error
    ? `${styles.input} ${styles.error}`
    : styles.input;
  const inputContainerClass = props.disabled
    ? `${styles.prefixContainer} ${styles.disabled}`
    : styles.prefixContainer;

  return (
    <div className={containerClass} style={style}>
      <label className={styles.textFieldLabel}>{label}</label>
      <div className={inputContainerClass}>
        <input placeholder={placeholder} {...field} {...props} />
      </div>
      {error ? <div className={styles.errorMessage}>{meta.error}</div> : null}
    </div>
  );
};

export default TextField;
