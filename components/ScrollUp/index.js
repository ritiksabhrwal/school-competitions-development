import styles from "./ScrollUp.module.css";
import { useEffect, useState } from "react";
import { ChevronDoubleUpIcon } from "@heroicons/react/24/outline";

const index = () => {
  const [visible, setVisible] = useState(false);

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 600) {
      setVisible(true);
    } else if (scrolled <= 600) {
      setVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toggleVisible);
  }, []);

  return (
    <button
      className={`${styles.scrollup} ${visible ? `${styles.showScroll}` : ""}`}
      id="scroll-up"
      onClick={scrollToTop}
    >
      <div className={styles.scrollupIcon}>
        <ChevronDoubleUpIcon className="icon" />
      </div>
    </button>
  );
};

export default index;
